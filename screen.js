// Canvas-State useful sets
// from: http://simonsarris.com/blog/510-making-html5-canvas-useful

function CanvasState(canvas) {

  var stylePaddingLeft, stylePaddingTop, styleBorderLeft, styleBorderTop;

  if (document.defaultView && document.defaultView.getComputedStyle) {
    this.stylePaddingLeft = parseInt(document.defaultView.getComputedStyle(canvas, null).paddingLeft, 10)     || 0;
    this.stylePaddingTop  = parseInt(document.defaultView.getComputedStyle(canvas, null).paddingTop, 10)      || 0;
    this.styleBorderLeft  = parseInt(document.defaultView.getComputedStyle(canvas, null).borderLeftWidth, 10) || 0;
    this.styleBorderTop   = parseInt(document.defaultView.getComputedStyle(canvas, null).borderTopWidth, 10)  || 0;
  }

  var html = document.body.parentNode;

  this.htmlTop = html.offsetTop;
  this.htmlLeft = html.offsetLeft;

}

CanvasState.prototype.getMouse = function(e) {

  var element = canvas, offsetX = 0, offsetY = 0, mx, my;

  if (element.offsetParent !== undefined) {
    do {
      offsetX += element.offsetLeft;
      offsetY += element.offsetTop;
    } while ((element = element.offsetParent));
  }

  offsetX += this.stylePaddingLeft + this.styleBorderLeft + this.htmlLeft;
  offsetY += this.stylePaddingTop + this.styleBorderTop + this.htmlTop;

  mx = e.pageX - offsetX;
  my = e.pageY - offsetY;

  return {x: mx, y: my};

};

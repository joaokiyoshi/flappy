// Flappy

var canvas, ctx, canvas_state, canvasX, canvasY, audios, frames = 0, score = 0;
var best = localStorage.getItem('best') || 0;
var current_state = 0;
var states = { menu: 0, splash: 1, game: 2, score: 3 };
var bg_pos = 0;
var buttons = { ok: {} };

var changeState = function (state) {
  current_state = states[state];
};

var bird = {
  x: 0,
  y: 0,
  frame: 0,
  velocity: 0,
  animation: [0, 1, 2, 1],
  rotation: 0,
  radius: 12,
  _gravity: 0.25,
  _jump: 4.6,

  jump: function () {
    //audios.wing.stop();
    //audios.wing.play();
    this.velocity = -this._jump;
  },

  update: function () {
    var index = current_state === states.splash ? 10 : 5;
    this.frame += frames % index === 0 ? 1 : 0;
    this.frame %= this.animation.length;
    if (current_state === states.menu) {
      this.x = canvasX / 2;
      this.y = canvasY / 2 + 5 * Math.sin(frames / 10);
    } else if (current_state === states.splash) {
      this.x = 80;
      this.y = canvasY - 280 + 5 * Math.sin(frames / 10);
      this.rotation = 0;
    } else {
      this.velocity += this._gravity;
      this.y += this.velocity;
      if (this.y >= canvasY - s_fg.height - 10) {
        this.y = canvasY - s_fg.height - 10;
        if (current_state === states.game) changeState('score');
        this.velocity = this._jump;
      }
      if (!(this.y >= canvasY - s_fg.height - 10)) {
        if (this.velocity >= this._jump) {
          this.frame = 1;
          this.rotation = Math.min(Math.PI / 2, this.rotation + 0.25);
        } else {
          this.rotation = -0.3;
        }
      }
    }
  },

  draw: function (ctx) {
    ctx.save();
    ctx.translate(this.x, this.y);
    ctx.rotate(this.rotation);
    ctx.strokeStyle = 'transparent';
    ctx.beginPath();
    ctx.arc(0, 0, this.radius, 0, 2 * Math.PI);
    ctx.stroke();
    var index = this.animation[this.frame];
    s_bird[index].draw(ctx, -s_bird[index].width / 2, -s_bird[index].height / 2);
    ctx.restore();
  }
};

var pipes = {

  _pipes: [],

  reset: function () {
    this._pipes = [];
  },

  update: function () {
    if (frames % 100 === 0) {
      var _y = canvasY - (s_pipeSouth.height + s_fg.height + 120 + 200 * Math.random ());
      this._pipes.push({
          x: 400,
          y: _y,
          width: s_pipeSouth.width,
          height: s_pipeSouth.height
      });
    }
    for (var i = 0, x = this._pipes.length; i < x; i++) {
      var pipe = this._pipes[i];
      if (i === 0) {
        if (pipe.x === bird.x) {
          //audios.bading.play();
          score += 1;
        }
        var cx = Math.min(Math.max(bird.x, pipe.x), pipe.x + pipe.width),
            cys = Math.min(Math.max(bird.y, pipe.y), pipe.y + pipe.height),
            cyn = Math.min(Math.max(bird.y, pipe.y + pipe.height + 90), pipe.y + 2 * pipe.height + 90),
            dx = bird.x - cx,
            dy1 = bird.y - cys,
            dy2 = bird.y - cyn,
            d1 = dx * dx + dy1 * dy1,
            d2 = dx * dx + dy2 * dy2,
            r = bird.radius * bird.radius;
        if (r > d1 || r > d2) {
          changeState('score');
        }
      }
      pipe.x -= 2;
      if (pipe.x < -50) {
        this._pipes.splice(i, 1);
        i--;
        x--;
      }
    }
  },

  draw: function (ctx) {
    this._pipes.forEach(function (pipe) {
      s_pipeSouth.draw(ctx, pipe.x, pipe.y);
      s_pipeNorth.draw(ctx, pipe.x, pipe.y + 90 + pipe.height);
    });
  }
};

var game = {

  init: function () {
    canvas = document.createElement('canvas');
    canvas_state = new CanvasState(canvas);
    canvasX = 300;
    canvasY = 480;
    canvas.width = canvasX;
    canvas.height = canvasY;
    canvas.style.border = '1px solid black';
    canvas.style.display = 'block';
    canvas.style.margin = 'auto';
    canvas.style.position = 'absolute';
    canvas.style.top = 0;
    canvas.style.bottom = 0;
    canvas.style.left = 0;
    canvas.style.right = 0;
    ctx = canvas.getContext('2d');

    // Click Events
    document.addEventListener('mousedown', function (evt) {
      switch (current_state) {

        case states.menu:
          changeState('splash');
          break;

        case states.splash:
          changeState('game');
          bird.jump();
          break;

        case states.game:
            bird.jump();
            break;

        case states.score:
          pipes.reset();
          score = 0;
          changeState('splash');
          break;
      }
    });

    document.body.appendChild(canvas);

    ctx.font = '20px Verdana';
    ctx.fillText('Loading...', 100, canvasY / 2 - 10);

    audios = {
      wing: new Audio(),
      point: new Audio(),
      hit: new Audio()
    };

    audios.wing.src = 'sfx_wing.ogg';
    audios.point.src = 'sfx_point.ogg';
    audios.hit.src = 'sfx_hit.ogg';

    var promises = [];

    Object.keys(audios).forEach(function (a) {
      promises.push(function () {
        return new Promise(function (resolve) {
          audios[a].onload = function () {
            resolve();
          };
        });
      });
    });

    var img = new Image();
    img.src = 'sheet.png';

    promises.push(function () {
      return new Promise(function (resolve) {
        img.onload = function () {
          resolve();
        };
      });
    });

    Promise.all(promises).then(function () {
      initSprites(img);
      game.main();
    });

  },

  main: function () {
    var loop = function () {
      game.loop();
      game.render();
      window.requestAnimationFrame(loop, canvas);
    };
    window.requestAnimationFrame(loop, canvas);
  },

  loop: function () {
    frames++;

    if (current_state !== states.score) {
        bg_pos = (bg_pos - 2) % 18;
    } else {
        best = Math.max(best, score);
        localStorage.setItem('best', best);
    }
    if (current_state === states.game) {
        pipes.update();
    }

    bird.update();
  },

  render: function () {
    ctx.fillStyle = '#72E5ED';
    ctx.fillRect(0, 0, canvasX, canvasY);

    s_bg.draw(ctx, 0, canvasY - s_bg.height);
    s_bg.draw(ctx, s_bg.width, canvasY - s_bg.height);

    pipes.draw(ctx);
    bird.draw(ctx);

    s_fg.draw(ctx, bg_pos, canvasY - s_fg.height);
    s_fg.draw(ctx, bg_pos + s_fg.width, canvasY - s_fg.height);

    if (current_state === states.menu) {
      s_text.FlappyBird.draw(ctx, canvasX / 2 - s_text.FlappyBird.width / 2, canvasY - 380);
    }

    if (current_state === states.splash) {
      s_splash.draw(ctx, canvasX / 2 - s_splash.width / 2, canvasY - 300);
      s_text.GetReady.draw(ctx, canvasX / 2 - s_text.GetReady.width / 2, canvasY - 380);
    }

    if (current_state === states.score) {
      s_text.GameOver.draw(ctx, canvasX / 2 - s_text.GameOver.width / 2, canvasY - 400);
      s_score.draw(ctx, canvasX / 2 - s_score.width / 2, canvasY - 340);
      s_buttons.Ok.draw(ctx, buttons.ok.x, buttons.ok.y);
      s_numberS.draw(ctx, canvasX / 2 - 47, canvasY - 304, score, null, 10);
      s_numberS.draw(ctx, canvasX / 2 - 47, canvasY - 262, best, null, 10);
      if (score >= 10 && score < 20) {
        s_medals.Bronze.draw(ctx, canvasX / 2 - 86, canvasY / 2 - 58);
      }
      if (score >= 20 && score < 30) {
        s_medals.Silver.draw(ctx, canvasX / 2 - 86, canvasY / 2 - 58);
      }
      if (score >= 30 && score < 40) {
        s_medals.Gold.draw(ctx, canvasX / 2 - 86, canvasY / 2 - 58);
      }
      if (score >= 40) {
        s_medals.Platinum.draw(ctx, canvasX / 2 - 86, canvasY / 2 - 58);
      }
    }

    if (current_state === states.game) {
      s_numberB.draw(ctx, canvasX / 2, 40, score, canvasX / 2);
    }
  }
};

window.onload = function () {
  game.init();
};
